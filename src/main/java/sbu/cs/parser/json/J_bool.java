package sbu.cs.parser.json;

public class J_bool extends J_elements
{
    private Boolean value;
    public void setValue(Boolean value)
    {
        this.value = value;
    }

    @Override
    public String getvalue()
    {
        return Boolean.toString(value);
    }
}
