package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface
{
    List<J_elements> els = new ArrayList<>();
    public Json(List<J_elements> els)
    {
        this.els = els;
    }

    @Override
    public String getStringValue(String key)
    {
        for(J_elements s : els)
        {
            if(s.getkey().equals(key))
            {
                return s.getvalue();
            }
        }
        return null;
    }
}
