package sbu.cs.parser.json;

public class J_int extends J_elements
{
    private int value;
    public void setvalue(int value)
    {
        this.value = value;
    }
    @Override
    public String getvalue()
    {
        return Integer.toString(value);
    }
}
