package sbu.cs.parser.json;

public class J_double extends J_elements
{
    private double value;
    public void setValue(double value)
    {
        this.value = value;
    }
    @Override
    public String getvalue()
    {
        return Double.toString(value);
    }
}
