package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    public static Json parse(String data)
    {
        List<J_elements> els = new ArrayList<>();

        data = data.replaceAll("(}|\\{|\"|\\s)", "");
        String temp = data;

        while (temp.indexOf('[') >= 0)
        {
            String s = temp.substring(temp.indexOf('['), temp.indexOf(']') + 1);
            data = data.replace(s, s.replaceAll(",", "#"));
            temp = temp.replace(s, "");
        }

        String[] splited = data.split(",");

        for(String s : splited)
        {
            String key = s.split(":")[0];
            String value = s.split(":")[1];

            if(value.matches("\\d*"))
            {
                J_int pivot = new J_int();
                pivot.setKey(key);
                pivot.setvalue(Integer.parseInt(value));
                els.add(pivot);
            }
            else if(value.matches("\\d*\\.\\d*"))
            {
                J_double pivot = new J_double();
                pivot.setKey(key);
                pivot.setValue(Double.parseDouble(value));
                els.add(pivot);
            }
            else if(value.matches("true|false"))
            {
                J_bool pivot = new J_bool();
                pivot.setKey(key);
                pivot.setValue(Boolean.parseBoolean(value));
                els.add(pivot);
            }
            else
            {
                J_string pivot = new J_string();
                pivot.setKey(key);
                pivot.setValue(value);
                els.add(pivot);
            }
        }
        Json parsedjson = new Json(els);
        return parsedjson;
    }
}
