package sbu.cs.parser.json;

public class J_string extends J_elements
{
    private String value;
    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public String getvalue()
    {
        if(value.indexOf('[') >= 0)
        {
            value = value.replaceAll("#",", ");
        }
        return value;
    }
}
